import json
import sys
import os
from datetime import datetime, timedelta

def start_session(logdir):
    """Start a session"""
    sessionfile = os.path.join(logdir,"active_session.json")
    if os.path.exists(sessionfile):
        sessiondata = json.load(open(sessionfile))
        if sessiondata["start"]:
            print(f"WARNING: You already have a running session, started at {sessiondata['start']}")
            validanswer = False
            while not validanswer:
                answer = input("Do you want to [d]iscard that session and start a new one or [A]bort? ")
                if answer == "A" or answer == "a" or answer == "":
                    print("Aborting")
                    sys.exit(0)
                elif answer == "D" or answer == "d":
                    print("Discarding previous session")
                    validanswer = True
                else:
                    print("Please enter either d or a as your choice")
    sessiondata = {"start": datetime.strftime(datetime.now(),"%Y-%m-%d %H:%M:%S")}
    json.dump(sessiondata, open(sessionfile, "w"))    

def stop_session(logdir, comment):
    """Stop a session"""
    sessionfile = os.path.join(logdir,"active_session.json")
    if not os.path.exists(sessionfile):
        print("No session is currently running, start one with `worktime start`")
        sys.exit(2)
    sessiondata = json.load(open(sessionfile))
    if not sessiondata["start"]:
        print("ERROR: Invalid active session file")
        sys.exit(3)
    session_start = datetime.strptime(sessiondata["start"],"%Y-%m-%d %H:%M:%S")
    session_end = datetime.now()
    
    if comment == None:
        sessiondata["comment"] = input("Add a comment to the session (optional): ")
    else:
        sessiondata["comment"] = comment
    
    sessiondata["end"] = datetime.strftime(session_end,"%Y-%m-%d %H:%M:%S")
    weekfile = os.path.join(logdir,datetime.strftime(session_start,"%Y-%W.json"))
    if os.path.exists(weekfile):
        weeklog = json.load(open(weekfile))
    else:
        weeklog = []
    weeklog.append(sessiondata)
    json.dump(weeklog,open(weekfile,"w"), indent=4)
    os.remove(sessionfile)

def stat_month(logdir,year,month):
    monthfile = os.path.join(logdir,f"{year}-{month:02}.json")
    month = json.load(open(monthfile))
    totaltime = timedelta()
    for session in month:
        start = datetime.strptime(session["start"], "%Y-%m-%d %H:%M:%S")
        end = datetime.strptime(session["end"], "%Y-%m-%d %H:%M:%S")
        sessiontime = end - start
        totaltime += sessiontime
    return totaltime

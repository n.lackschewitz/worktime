#!/usr/bin/env python3
import os
import sys
import json
import utils
from sessionhandling import *

config_dir = utils.get_user_config_directory()

if not config_dir:
    print("Could not find a configuration directory.")
    print("If you are on windows, make sure LOCALAPPDATA or APPDATA is set!")

config_filename = os.path.join(config_dir, "worktime.json")
if os.path.exists(config_filename):
    config_filehandle = open(config_filename, "r+")
else:
    config_filehandle = open(config_filename, "w+")

try:
    config = json.load(config_filehandle)
except json.decoder.JSONDecodeError:
    config = {"worklogs_directory": os.path.join(os.path.dirname(os.path.realpath(__file__)),"worklogs")}
    json.dump(config,config_filehandle)

usage_instructions = """
Usage:
`worktime start [time]` starts a new session.
Time can be one of the following:
 - An offset from now, prefixed with a + or -, either in the format "HH:MM" or suffixed with one of "m","min","mins","minutes","h" or "hours"
 - A time today, specified as "HH:MM" (24-hour format) or "HH:MM am"/"HH:MM pm" (12-hour format). Leading zeroes and the space before "am" or "pm" may be omitted. 
 - A date and time, specifying first the date in "yyyy-mm-dd" format and then the time as above. The century part of the year and any leading zeroes except in the remaining two digits of the year may be omitted.
If no time is specified, the session starts at the time this command is executed.

`worktime stop [time]` stops a running session.
Time specification is identical to starting a session.

`worktime report <format name> [month] [year] <output filename>` generates a report.
Month is mandatory if year is specified, month and year both default to the current one.

WARNING: Report formats can contain arbitrary code that is executed when generating a report from that format, only use report formats from sources you trust!
"""

if len(sys.argv)==1:
    print(usage_instructions)
    sys.exit(0)

if sys.argv[1] == "start":
    start_session(config["worklogs_directory"])
elif sys.argv[1] == "stop":
    comment_index = -1
    if "-m" in sys.argv:
        comment_index = sys.argv.index("-m")+1
    elif "-c" in sys.argv:
        comment_index = sys.argv.index("-c")+1
    elif "-comment" in sys.argv:
        comment_index = sys.argv.index("--comment")+1
    
    if comment_index == -1:
        comment = None
    elif comment_index >= len(sys.argv):
        comment = ""
    else:
        comment = sys.argv[comment_index]
    stop_session(config["worklogs_directory"], comment)
elif sys.argv[1] == "report":
    print("ERROR: This is not yet implemented. Hold on to your worklogs, report generation will be there eventually")
elif sys.argv[1] == "stat":
    now = datetime.now()
    month_text = "this month"
    month = now.month
    year = now.year
    if len(sys.argv)>2:
        if sys.argv[2].isnumeric():
            month = int(sys.argv[2])
            if month>12:
                print(f"Invalid month number: {sys.argv[2]}")
                sys.exit(1)
        else:
            print(f"Invalid month number: {sys.argv[2]}")
            sys.exit(1)
        if len(sys.argv)>3:
            if sys.argv[3].isnumeric():
                year = int(sys.argv[3])
            else:
                print(f"Invalid month number: {sys.argv[2]}")
                sys.exit(1)
        month_text = f"in {year}-{month}"
    elif os.path.exists(os.path.join(config["worklogs_directory"],"active_session.json")):
        month_text += " (excluding currently running session)"
    totaltime = int(stat_month(config['worklogs_directory'],year,month).total_seconds())
    print(f"Time worked {month_text}: {totaltime//3600:02}:{(totaltime//60)%60:02}")
elif sys.argv[1] == "info":
    if len(sys.argv)<3:
        print("Sub-operation required for `info`")
        sys.exit(1)
    elif sys.argv[2] == "prompt":
        sessionfile = os.path.join(config["worklogs_directory"],"active_session.json")
        if os.path.exists(sessionfile):
            sessiondata = json.load(open(sessionfile))
            if sessiondata["start"]:
                timediff = int((datetime.now()-datetime.strptime(sessiondata["start"],"%Y-%m-%d %H:%M:%S")).total_seconds())
                print(f" ({timediff//3600:02}:{(timediff//60)%60:02})", end="")
    else:
        print(f"Unknown sub-operation {sys.argv[2]}")
        sys.exit(1)

else:
    print(f"Unknown operation {sys.argv[1]}")
    print(usage_instructions)
    sys.exit(1)

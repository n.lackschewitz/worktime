# Worktime (für die Workloaderhebung 2. Semester)
## Einrichtung
### Linux
 - Klone das git-repository mit `git clone https://gitlab.gwdg.de/n.lackschewitz/worktime`
 - Kopiere das `worktime`-script aus dem repository in einen Ordner, der bei dir im PATH ist, z.B. `~/.local/bin` sollte meist funktionieren.
 - In der Kopie, trage das Verzeichnis ein, in das du dieses repository geklont hast
 - Führe `worktime` einmal in deinem Terminal aus. Wenn du Hinweise zur Benutzung siehst hat die
   Einrichtung funktioniert.
 - Im Hintergrund legt das Programm dabei eine Datei `worktime.json` an (in `XDG_CONFIG_HOME`, or falls diese Variable nicht gesetzt ist in `~/.config`). In dieser Datei könntest du einstellen, wo gespeichert wird welche Zeiten du einträgst, das ist aber eigentlich nicht nötig.
### Andere Betriebssysteme
Sollte ähnlich wie bei Linux funktionieren, aber du müsstest das Wrapper-Script selber schreiben,
ich habe keines geschrieben weil ich es nicht testen kann. Der Code selbst ist aber möglichst
system-unabhängiger python-code, also solange python auf deinem PC läuft müsste auch dieses Programm
funktionieren.

## Benutzung
Mit `worktime start` kannst du eintragen, dass du angefangen hast zu arbeiten, und mit `worktime
stop`, dass du aufgehört hast. (Tipp: Lege dir kürzere aliase auf diese Befehle)
Nachdem du `worktime stop` eingegeben hast wirst du nach einem Kommentar gefragt, trage da ein, für
welches Modul du in dieser Zeit gearbeitet hast.

## Deine Aufzeichnungen hochladen
In den Umfragen während des Semesters wirst du immer wieder gebeten, deine Zeitaufzeichnungen zu
teilen. Diese findest du standardmäßig im Ordner `worklogs`, der ein Unterordner von dem Ordner ist,
in den du dieses repository geklont hast. Jede Datei dort entspricht einer Woche, numeriert mit der
Jahreszahl und der Kalenderwoche.
